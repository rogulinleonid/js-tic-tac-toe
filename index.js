var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';

let sizeMap;
let crossMove;
let countMove;
let gameOver;
let mapGame;


startGame();

function startGame () {
  crossMove = true;
  gameOver = false;
  countMove = 0;
  sizeMap =  parseInt(prompt('Введите размер игрового поля', 3));
  renderGrid(sizeMap);

  mapGame = new Array(sizeMap);
for (let i = 0; i < mapGame.length; i++) {
  mapGame[i] = new Array(sizeMap);
  for (let j = 0; j < mapGame[i].length; j++) {
    mapGame[i][j] = 0;
  }
}
}



/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  if (gameOver) {
    return 0;
  }
countMove++;
if (mapGame[row][col] === 0) {
  if (crossMove) {
    mapGame[row][col] = 1;
    renderSymbolInCell(CROSS, row, col);
    crossMove = !crossMove;
  }
  else {
    mapGame[row][col] = 2;
    renderSymbolInCell(ZERO, row, col);
    crossMove = !crossMove;
    console.log(`Clicked on cell: ${row}, ${col}`);
  }
  if (checkWinner(mapGame, sizeMap)) {
    let winner = crossMove ? 'нолики' : 'крестики';
    showMessage(`${winner} победили!`)
    gameOver = true;
    return 0;
  }
  if (countMove === sizeMap * sizeMap) {
    gameOver = true;
    showMessage('Победила дружба');
  }
}
  /* Пользоваться методом для размещения символа в клетке так:
      renderSymbolInCell(ZERO, row, col);
   */
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  startGame();
  for (let i = 0; i < sizeMap; i++) {
    for (let j = 0; j < sizeMap; j++) {
      mapGame[i][j] = 0;
      renderSymbolInCell(EMPTY, i, j); 
    }
  }
  showMessage('');
  console.log('reset!');
}

function checkWinner(arr, length) {
  length = length || arr.length;
  var winner = [
     arr.map(row=>row.join('')).join(' '),
     arr.map((_,v)=>arr.map(row=>row[v]).join('')).join(' '),
     arr.map((_,v)=>arr[v][v]).join(''),
     arr.map((_,v)=>arr[v][arr.length - v - 1]).join(''),
  ].join(' ').match('1'.repeat(length) + '|' + '2'.repeat(length));

  return +((winner || ['0'])[0][0]);
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
